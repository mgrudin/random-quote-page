import React from 'react';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      quotes: {},
      quote: {
        author: null,
        text: null,
      },
    };
  }

  componentDidMount() {
    const url =
      'https://gist.githubusercontent.com/camperbot/5a022b72e96c4c9585c32bf6a75f62d9/raw/e3c6895ce42069f0ee7e991229064f167fe8ccdc/quotes.json';

    let response = fetch(url)
      .then((res) => res.json())
      .then(
        (result) => {
          console.log(result);
          const quote =
            result.quotes[Math.floor(Math.random() * result.quotes.length)];

          this.setState({
            quotes: result,
            quote: {
              author: quote.author,
              text: quote.quote,
            },
          });
        },
        (error) => {
          console.log('HTTP Error: ' + response.status);
          return;
        }
      );
  }

  createNewQuote() {
    const quote = this.state.quotes.quotes[
      Math.floor(Math.random() * this.state.quotes.quotes.length)
    ];

    this.setState({
      quote: {
        author: quote.author,
        text: quote.quote,
      },
    });
  }

  render() {
    return (
      <div className="App">
        <div id="quote-box">
          <blockquote id="text">{this.state.quote.text}</blockquote>
          <div id="author">{this.state.quote.author}</div>
          <div className="quote-box__footer">
            <a
              href={
                'https://twitter.com/intent/tweet?hashtags=quotes&related=freecodecamp&text=' +
                encodeURIComponent(
                  '"' + this.state.quote.text + '" ' + this.state.quote.author
                )
              }
              target="_blank"
              rel="noopener noreferrer"
              id="tweet-quote"
            >
              <img src="icon_twitter.png" alt="" width="32" height="32" />
            </a>
            <button
              type="button"
              id="new-quote"
              onClick={(e) => this.createNewQuote(e)}
            >
              New quote
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
